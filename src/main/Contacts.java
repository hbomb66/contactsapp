package main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * Dead simple contacts GUI with JDBC
 * @author Helen
 *
 */
public class Contacts extends JFrame{
	
	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:oracle:thin:@localhost:1521:ORCL";
	private static final String DB_USER = "scott";
	private static final String DB_PASSWORD = "tiger";
	
	//Add Contact Frame
	public JFrame addContactFrame;
	public JButton saveNewContactBtn;
	public JButton cancelContactFormBtn;
	public JPanel contactButtonPanel = new JPanel();
	public JPanel contactFieldsPanel = new JPanel();
	public JLabel nameFieldLabel = new JLabel("Name:");
	public JTextField nameField = new JTextField(15);
	public JLabel addressFieldLabel = new JLabel("Address:");
	public JTextField addressField = new JTextField(15);
	public JLabel phoneFieldLabel = new JLabel("Phone:");
	public JTextField phoneField = new JTextField(10);
	public JLabel emailFieldLabel = new JLabel("Email:");
	public JTextField emailField = new JTextField(10);

	public GridLayout aGridLayout = new GridLayout(0,2);

	//Main Frame
	public JFrame frame = new JFrame("Contacts");
	public JButton addNewContact = new JButton("Add New Contact");
	public static DefaultListModel <String>contactsListModel = new DefaultListModel <String>();
	public JList <String> contactsArea = new JList<String>(contactsListModel);
	public JScrollPane scroll;
	public JPanel mainBtnPanel = new JPanel();
	public JPanel topPanel = new JPanel();
	public JPanel searchPanel = new JPanel();
	public JButton clearBtn = new JButton("Clear");
	public JButton searchBtn = new JButton("Search");
	public JTextField searchField = new JTextField(10);
	

	//Customized contact frame
	public JLabel cnameFieldLabel = new JLabel("Name:");
	public JTextField cnameField = new JTextField(10);
	public JLabel caddressFieldLabel = new JLabel("Address:");
	public JTextField caddressField = new JTextField(10);
	public JLabel cphoneFieldLabel = new JLabel("Phone:");
	public JTextField cphoneField = new JTextField(10);
	public JLabel cemailFieldLabel = new JLabel("Email:");
	public JTextField cemailField = new JTextField(10);
	public JPanel ccontactFieldsPanel = new JPanel();
	public JFrame contactJFrame = new JFrame();
	public JPanel contactBtnPanel = new JPanel();
	public JButton deleteContact = new JButton("Delete Contact");
	public JButton save = new JButton("Save");
	public JButton cancel = new JButton("Cancel");

	public Contacts() {
		initGUI();
	}
	
	public void initGUI(){
		
		//Main Frame
		mainBtnPanel.setLayout(new BorderLayout());
		mainBtnPanel.add(addNewContact);
		contactsArea.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		scroll = new JScrollPane(contactsArea);
		scroll.setPreferredSize(new Dimension(280, 190)); //set JList size here
		scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
		
		topPanel.add(scroll);
		
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.LINE_AXIS));
		searchPanel.add(searchField);
		searchPanel.add(searchBtn);
		searchPanel.add(clearBtn);
		frame.add(searchPanel, BorderLayout.NORTH);
		frame.add(topPanel, BorderLayout.CENTER);
		frame.add(mainBtnPanel, BorderLayout.SOUTH);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300,300);
		
		
		//Customized Contact Frame
		ccontactFieldsPanel.setLayout(aGridLayout);
		ccontactFieldsPanel.add(cnameFieldLabel, BorderLayout.WEST);
		ccontactFieldsPanel.add(cnameField, BorderLayout.EAST);
		ccontactFieldsPanel.add(caddressFieldLabel, BorderLayout.WEST);
		ccontactFieldsPanel.add(caddressField);
		ccontactFieldsPanel.add(cemailFieldLabel, BorderLayout.WEST);
		ccontactFieldsPanel.add(cemailField);
		ccontactFieldsPanel.add(cphoneFieldLabel, BorderLayout.WEST);
		ccontactFieldsPanel.add(cphoneField, BorderLayout.EAST);

		ccontactFieldsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		contactBtnPanel.setLayout(new BoxLayout(contactBtnPanel, BoxLayout.LINE_AXIS)); //to line them all up on the bottom
		contactBtnPanel.add(save);
		contactBtnPanel.add(deleteContact);
		contactBtnPanel.add(cancel);

		contactJFrame.setLocationRelativeTo(null);
		contactJFrame.add(ccontactFieldsPanel, BorderLayout.NORTH);
		contactJFrame.add(contactBtnPanel, BorderLayout.SOUTH);
		contactJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contactJFrame.setSize(300,300);
		contactJFrame.setVisible(false);
				

		//Add Contact Frame
		addContactFrame = new JFrame("Add New Contact");

		saveNewContactBtn = new JButton("Save");
		cancelContactFormBtn = new JButton("Close");

		contactButtonPanel.add(saveNewContactBtn);
		contactButtonPanel.add(cancelContactFormBtn);

		contactFieldsPanel.setLayout(aGridLayout);
		contactFieldsPanel.add(nameFieldLabel, BorderLayout.WEST);
		contactFieldsPanel.add(nameField, BorderLayout.EAST);
		contactFieldsPanel.add(addressFieldLabel, BorderLayout.WEST);
		contactFieldsPanel.add(addressField);
		contactFieldsPanel.add(emailFieldLabel, BorderLayout.WEST);
		contactFieldsPanel.add(emailField);
		contactFieldsPanel.add(phoneFieldLabel, BorderLayout.WEST);
		contactFieldsPanel.add(phoneField, BorderLayout.EAST);
		contactFieldsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		addContactFrame.add(contactFieldsPanel, BorderLayout.NORTH);
		addContactFrame.add(contactButtonPanel, BorderLayout.SOUTH);
		addContactFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addContactFrame.setSize(300,300);
		addContactFrame.setLocationRelativeTo(null);
		addContactFrame.setVisible(false);
				

		/**
		 * Main Frame's actions
		 */
		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contactsListModel.removeAllElements();
				String query = searchField.getText();
				
				if (isInteger(query)) {
					try {
						searchNum(Integer.parseInt(query));
					} catch (SQLException se) {
						se.printStackTrace();
					}
				} else {
					try {
						searchString(query);
					} catch (SQLException se) {
						se.printStackTrace();
					}
				}
			}
		});
		
		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchField.setText("");
				contactsListModel.clear();
				try {
					populateJList();
				} catch (SQLException se) {
					se.printStackTrace();
				}
			}
		});
		
		//when double clicked
		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					String name = (String) contactsArea.getSelectedValue();
					try {
						initContactFrame(name);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				} 
			}
		};
		
		contactsArea.addMouseListener(mouseListener);
		
		
		/**
		 * Add New Contact frame's actions
		 */
		addNewContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				addContactFrame.setVisible(true);
				frame.setVisible(false);
	
			}
		});
		
		cancelContactFormBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				addContactFrame.setVisible(false);
				frame.setVisible(true);
	
			}
		});
		
		saveNewContactBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = nameField.getText();
				String address = addressField.getText();
				String email = emailField.getText();
				String phone = phoneField.getText();

				try {
					addNewContact(name, phone, email, address);
					contactsListModel.clear();
					populateJList();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		        addContactFrame.setVisible(false);
				frame.setVisible(true);
		        
		        //resetting all fields
		        nameField.setText("");
		        addressField.setText("");
		        emailField.setText("");
		        phoneField.setText("");

			}
		});
		
		
		/**
		 * Specific Contact JFrame's actions
		 */
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contactJFrame.setVisible(false);
				
			}
			
		});
		
		deleteContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String selected = (String) contactsArea.getSelectedValue();
					deleteContact(selected);
					
					contactsListModel.removeAllElements();
					populateJList();
					contactJFrame.setVisible(false);
				} catch (SQLException se) {
					se.printStackTrace();
				}
			}
		});
		
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = cnameField.getText();
				String address = caddressField.getText();
				String email = cemailField.getText();
				String phone = cphoneField.getText();
				
				try {
					updateContact(name, phone, email, address);
			        contactsListModel.removeAllElements();
			        populateJList();
			        contactJFrame.setVisible(false);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			
		});
	}
	
	
	/**
	 * Fills in all of the customized JFrame's fields
	 */
	public void initContactFrame(String name) throws SQLException {
		
		Connection con = null;
		PreparedStatement pst = null;
		
		con = getDBConnection();
				
		try {			
			pst = con.prepareStatement(
					"select phone, email, address from contacts where name = ?",
					ResultSet.TYPE_SCROLL_SENSITIVE
					);

			pst.setString(1, name);
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) { //important!
				String phone  = rs.getString("PHONE");
				String email = rs.getString("EMAIL");
				String address = rs.getString("Address");

				cnameField.setText(name);
				cphoneField.setText(phone);
				caddressField.setText(address);
				cemailField.setText(email);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			pst.close();
			con.close();
		}

		contactJFrame.setVisible(true);
		
	}
	
	public static void populateJList() throws SQLException {
		
		Connection con = null;
		Statement st = null;
		
		con = getDBConnection();
		
		String query = "select name from contacts order by 1";
		
		try {
			st = con.createStatement();
			
			ResultSet rs = st.executeQuery(query);
			
			while (rs.next()) {
				String name = rs.getString("NAME");
				contactsListModel.addElement(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) st.close();
			con.close();
		}
	}
	
	public static void searchString (String query) throws SQLException {
		Connection con = null;
		PreparedStatement pst = null;
		
		con = getDBConnection();
				
		try {			
			pst = con.prepareStatement(
					"select * from contacts where name = ? or email = ? or address = ? order by 1",
					ResultSet.TYPE_SCROLL_SENSITIVE
					);

			pst.setString(1, query);
			pst.setString(2, query);
			pst.setString(3, query);
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) { //important!		
				String name = rs.getString("NAME");
				contactsListModel.addElement(name);
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			pst.close();
			con.close();
		}
		
	}
	
	public static void searchNum (int query) throws SQLException {
		Connection con = null;
		PreparedStatement pst = null;
		
		con = getDBConnection();
				
		try {			
			pst = con.prepareStatement(
					"select * from contacts where phone = ? order by 1",
					ResultSet.TYPE_SCROLL_SENSITIVE
					);

			pst.setInt(1, query);
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) { //important!		
				String name = rs.getString("NAME");
				contactsListModel.addElement(name);
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			pst.close();
			con.close();
		}
	}
	
	public static void updateContact (String name, String phone, String email, String address) throws SQLException {
		Connection con = null;
		PreparedStatement pst = null;
		
		con = getDBConnection();
		
		try {
			pst = con.prepareStatement(
					"update contacts " +
					"set phone = ?," +
					"email = ?," +
					"address = ?" +
					"where name = ?"
					);
			
		    pst.setString(1, phone);
		    pst.setString(2, email);
		    pst.setString(3, address);
		    pst.setString(4, name);
			pst.executeUpdate();

		} finally {
			if (pst != null) pst.close();
			con.close();
		}
	
	}
	
	public static void deleteContact (String name) throws SQLException {
		
		System.out.println("deleted");
		Connection con = null;
		PreparedStatement pst = null;
		
		con = getDBConnection();
				 
		try {
			pst = con.prepareStatement("delete from CONTACTS where name = ?");
			pst.setString(1, name);
			pst.executeUpdate();
			con.commit(); //important

		} catch (Exception se) {
			se.printStackTrace();
		}
		finally {
			if (pst != null) pst.close();
			con.close();
		}
	}
	
	public static void addNewContact (String name, String phone, String email, String address) throws SQLException {
		
		Connection con = null;
		PreparedStatement pst = null;
		
		con = getDBConnection();

		try {
			pst = con.prepareStatement(
					"insert into contacts " +
					"values (?,?,?,?)"
					);
			
			pst.setString(1, name);
		    pst.setString(2, phone);
		    pst.setString(3, email);
		    pst.setString(4, address);
			pst.executeUpdate();

		} finally {
			if (pst != null) pst.close();
			con.close();
		}
	}

	public static void main(String[] args) {
		
		System.out.println("Initiated");
		
		try {
			populateJList();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Contacts contactsGUI = new Contacts();
	
	}
	
	private static Connection getDBConnection() {
		 
		Connection dbConnection = null;
 
		try {
			Class.forName(DB_DRIVER);
 
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
 
		}
 
		try {
			dbConnection = DriverManager.getConnection(
                            DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;
 
		} catch (SQLException e) {
			System.out.println(e.getMessage());
 
		}
 
		return dbConnection;
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}
}
